package ve.net.dcs.sc.test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SCTest {

	static int scint;
	static int ecint;
	static int posts;
	static int poste;
	static int dec;
	static char sc;
	static char ec;
	static int stabilityPos;
	static boolean isStableValue;
	static int stabilityValue;
	

	public static String getValue(String string) {
		String patternString = String.format("\\%c([^\\%c\\%c]+)\\%c", sc, sc, ec, ec);
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(string);
		HashMap<String, Integer> list = new HashMap<String, Integer>();

		String raw = "";
		String value = "";
		String stability = "";

		while (matcher.find()) {
			raw = matcher.group(0);
			stability = raw.substring(stabilityPos, stabilityPos + 1);
			value = raw.substring(posts, poste + 1).replaceAll("[^0-9]", "");
			if (dec > 0)
				value = String.valueOf(Integer.parseInt(value) / Math.pow(10, dec));
			value = stability + value;
			if (list.containsKey(value)) {
				list.put(value, list.get(value) + 1);
			} else {
				list.put(value, 1);
			}
		}

		Iterator<String> iterator = list.keySet().iterator();

		int max = 0;
		String keyMax = "";

		while (iterator.hasNext()) {
			String stringTemp = (String) iterator.next();
			if (list.get(stringTemp) >= max) {
				max = list.get(stringTemp);
				keyMax = stringTemp;
			}
		}

		return keyMax;
	}

	public static String readValue(String string, int readings) {
		HashMap<String, Integer> list = new HashMap<String, Integer>();

		for (int i = 0; i < readings; i++) {
			String value = getValue(string);
			if (list.containsKey(value)) {
				list.put(value, list.get(value) + 1);
			} else {
				list.put(value, 1);
			}
		}
		Iterator<String> iterator = list.keySet().iterator();

		int max = 0;
		String keyMax = "";

		while (iterator.hasNext()) {
			String stringTemp = (String) iterator.next();
			if (list.get(stringTemp) >= max) {
				max = list.get(stringTemp);
				keyMax = stringTemp;
			}
		}
		
		if (keyMax.charAt(0) == stabilityValue) {
			isStableValue = true;
		} else {
			isStableValue = false;
		}

		return keyMax.substring(1);
	}

	public static void main(String[] args) {
		scint = 2;
		ecint = 13;
		posts = 4;
		poste = 9;
		sc = (char) scint;
		ec = (char) ecint;
		dec = 0;
		stabilityPos = 2;
		stabilityValue = '3';

		String string = String.format("%c53  17097  0000%cF%c53  17097  0000%cF%c50  17092  0000%cF%c50  17095  0000%cF", sc, ec, sc, ec, sc, ec, sc, ec);

		System.out.println(readValue(string, 2));
		System.out.println(isStableValue);

	}

}

/**
 * 
 * Scale Connector
 * 
 * Copyright (C) Double Click Sistemas C.A. RIF: J-31576020-7 
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Double Click Sistemas C.A. Barquisimeto, Venezuela, http://dcs.net.ve
 * 
 */

package ve.net.dcs.sc.feature;

import java.util.Enumeration;
import java.util.Properties;

/**
 * This class contains information about <b>Scale Connector</b>
 * 
 * @author Double Click Sistemas C.A. - http://dcs.net.ve
 * @author Saul Pina - spina@dcs.net.ve
 */
public class SCFeature {

	public static final String APP_NAME = "Scale Connector";
	public static final String VENDOR = "Double Click Sistemas C.A.";
	public static final String WEB = "http://dcs.net.ve";
	public static final String VERSION = "2.0.1.B";

	private static Properties properties = new Properties();

	static {
		properties.put("VENDOR", VENDOR);
		properties.put("WEB", WEB);
		properties.put("APP_NAME", APP_NAME);
		properties.put("VERSION", VERSION);
		properties.put("OS", System.getProperty("os.name"));
	}

	/**
	 * @param key
	 *            The key whose associated value is to be returned
	 * @return Setting value
	 * @see SCFeature#set(String, String)
	 */
	public static String get(String key) {
		return properties.getProperty(key);
	}

	/**
	 * @return An enumeration of the keys in the settings
	 */
	public static Enumeration<Object> getKeys() {
		return properties.keys();
	}

	/**
	 * @param key
	 *            The key whose associated setting value
	 * @param value
	 *            Setting
	 * @see SCFeature#get(String)
	 */
	public static void set(String key, String value) {
		properties.put(key, value);
	}

}

package ve.net.dcs.sc.ui.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Saul Pina - spina@dcs.net.ve
 */
public abstract class HelperDate {
	private static SimpleDateFormat sdf;

	public static String format(Date date, String format) {
		sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	public static Date now() {
		return Calendar.getInstance().getTime();
	}

	public static String nowFormat(String format) {
		sdf = new SimpleDateFormat(format);
		return sdf.format(Calendar.getInstance().getTime());
	}

	public static Date future(int field, int quantity) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(field, quantity);
		return calendar.getTime();
	}

	public static Date future(Calendar calendar, int field, int quantity) {
		Calendar calendarClone = (Calendar) calendar.clone();
		calendarClone.add(field, quantity);
		return calendarClone.getTime();
	}

}

/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/

package ve.net.dcs.sc.idempiere.process;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.HashMap;

import ve.net.dcs.sc.component.Client;
import ve.net.dcs.sc.component.Request;
import ve.net.dcs.sc.component.RequestType;
import ve.net.dcs.sc.component.Response;
import ve.net.dcs.sc.component.ResponseStatus;
import ve.net.dcs.sc.idempiere.model.MSCScale;
import ve.net.dcs.sc.idempiere.util.HelperDate;

/**
 * This class allows you to connect to the scale
 * 
 * @author Double Click Sistemas C.A. - http://dcs.net.ve
 * @author Saul Pina - spina@dcs.net.ve
 */
public class ScaleConnectorClient {

	private MSCScale recordScale;
	private Response response;

	/**
	 * Set Scale Settings
	 * 
	 * @param recordScale
	 *            MSCScale
	 */
	public ScaleConnectorClient(MSCScale recordScale) {
		this.recordScale = recordScale;
	}

	/**
	 * Sends a request to the server, the request must contain all information
	 * necessary to obtain the desired response
	 * 
	 * @return The response obtained
	 * @throws SocketTimeoutException
	 *             If timeout expires before connecting
	 * @throws IOException
	 *             If an error occurred when reading from the input stream
	 * @throws ClassNotFoundException
	 *             If the returned object is of a class unknown
	 */
	public Response sendRequest() throws SocketTimeoutException, ClassNotFoundException, IOException {
		HashMap<String, String> parameters = new HashMap<String, String>();

		parameters.put("baud", String.valueOf(recordScale.getBaud()));
		parameters.put("serialport", recordScale.getSerialPort());
		parameters.put("stopbits", String.valueOf(recordScale.getStopBits()));
		parameters.put("parity", String.valueOf(recordScale.getParity()));
		parameters.put("databits", String.valueOf(recordScale.getDataBits()));
		parameters.put("bytecount", String.valueOf(recordScale.getByteCount()));
		parameters.put("startcharacter", String.valueOf(recordScale.getStartCharacter()));
		parameters.put("endcharacter", String.valueOf(recordScale.getEndCharacter()));
		parameters.put("readings", String.valueOf(recordScale.getReadings()));
		parameters.put("startcut", String.valueOf(recordScale.getStartCutPosition()));
		parameters.put("endcut", String.valueOf(recordScale.getEndCutPosition()));
		parameters.put("stabilitypos", String.valueOf(recordScale.getStabilityPosition()));
		parameters.put("stability", String.valueOf(recordScale.getStability()));
		parameters.put("floatingpoint", String.valueOf(recordScale.getFloatingPoint()));

		Client client = new Client(recordScale.getSC_ServerSettings().getHostAddress(), recordScale.getSC_ServerSettings().getHostPort());
		client.setTimeout(recordScale.getSC_ServerSettings().getSecondsTimeout() * 1000);
		client.setWebService(recordScale.getSC_ServerSettings().isWebService());
		Request request = new Request();
		request.setType(RequestType.READ_PORT);
		request.setDate(HelperDate.now());
		request.setParameters(parameters);
		response = client.sendRequest(request);
		return response;
	}
	
	public ResponseStatus getResposeStatus(){
		return response.getStatus();
	}

	public double getValue() {

		if (response.getValue("value") == null) {
			return 0;
		}
		if (!response.getValue("value").matches("[0-9\\.]+")) {
			return 0;
		}
		try {
			return Double.parseDouble(response.getValue("value"));
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public Response getResponse() {
		return response;
	}

	public boolean getStability() {
		return Boolean.parseBoolean(response.getValue("isstable"));
	}

	public String getStringValue() {
		return response.getValue("value");
	}
}

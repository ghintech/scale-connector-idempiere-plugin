/**
 * 
 * Scale Connector
 * 
 * Copyright (C) Double Click Sistemas C.A. RIF: J-31576020-7 
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Double Click Sistemas C.A. Barquisimeto, Venezuela, http://dcs.net.ve
 * 
 */

package ve.net.dcs.sc.component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.Iterator;
import java.util.logging.Logger;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Send a request to the Scale Server. <b>Example:</b>
 * <p>
 * 
 * <pre>
 * Client client = new Client(&quot;localhost&quot;, 5000);
 * client.setTimeout(1000);
 * Request request = new Request();
 * request.setType(RequestType.TEST);
 * request.setDate(Calendar.getInstance().getTime());
 * Response response = client.sendRequest(request);
 * </pre>
 * 
 * </p>
 * 
 * @author Double Click Sistemas C.A. - http://dcs.net.ve
 * @author Saul Pina - spina@dcs.net.ve
 * @see Request
 * @see Response
 * @see RequestType
 */
public class Client {

	private static Logger logger = Logger.getLogger(Client.class.getName());

	private int port;
	private String host;
	private int timeout;
	private boolean isWebService;
	private String protocol;
	private String uri;

	/**
	 * @return Web Service Protocol
	 */
	public String getProtocol() {
		return protocol;
	}

	/**
	 * Set protocol
	 * 
	 * @param protocol
	 *            Web Service Protocol
	 */
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	/**
	 * @return Web Service URI
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * Set Web Service URI
	 * 
	 * @param uri
	 *            Web Service URI
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}

	/**
	 * @return Port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port
	 *            Port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * @return Host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host
	 *            Host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return Timeout, default 10000
	 */
	public int getTimeout() {
		return timeout;
	}

	/**
	 * Set timeout, default 10000
	 * 
	 * @param timeout
	 *            Timeout to set
	 */
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	/**
	 * @return True: If the remote host is web service
	 */
	public boolean isWebService() {
		return isWebService;
	}

	/**
	 * Set type server
	 * 
	 * @param isWebService
	 */
	public void setWebService(boolean isWebService) {
		this.isWebService = isWebService;
	}

	/**
	 * @param host
	 *            The Host name
	 * @param port
	 *            The Port number
	 */
	public Client(String host, int port) {
		this.port = port;
		this.host = host;
		timeout = 10000;
		protocol = "http";
		uri = "/scale_connector_ws/get_response";
	}

	/**
	 * Sends a request to the server, the request must contain all information
	 * necessary to obtain the desired response
	 * 
	 * @param request
	 *            The request to send
	 * @return The response obtained
	 * @throws SocketTimeoutException
	 *             If timeout expires before connecting
	 * @throws IOException
	 *             If an error occurred when reading from the input stream
	 * @throws ClassNotFoundException
	 *             If the returned object is of a class unknown
	 * @see <a href="http://code.google.com/p/json-simple/">
	 *      http://code.google.com/p/json-simple/</a>
	 */
	public Response sendRequest(Request request) throws SocketTimeoutException, IOException, ClassNotFoundException {
		logger.info("Connecting to server");
		Response response = null;
		if (isWebService) {

			String parameters = "requestType=" + request.getType().toString();

			Iterator<String> i = request.getParameters().keySet().iterator();

			while (i.hasNext()) {
				String p = i.next();
				parameters += "&" + p + "=" + request.getParameter(p);
			}

			String url = String.format("%s://%s:%s%s?%s", protocol, host, port, uri, parameters);

			URL urlInput = new URL(url);
			URLConnection con = urlInput.openConnection();
			con.setConnectTimeout(timeout);
			con.setReadTimeout(timeout);

			BufferedReader buffer = new BufferedReader(new InputStreamReader(con.getInputStream(), Charset.forName("ISO-8859-1")));
			String temp = "";
			String file = "";
			while ((temp = buffer.readLine()) != null) {
				file = file + temp;
			}

			JSONParser parser = new JSONParser();

			try {
				JSONObject obj = (JSONObject) parser.parse(file);
				JSONObject objData = (JSONObject) parser.parse(obj.get("data").toString());

				response = new Response();
				response.setServerMessage(obj.get("serverMessage").toString());
				response.setDate(Calendar.getInstance().getTime());
				response.setStatus(ResponseStatus.valueOf(obj.get("status").toString().toUpperCase()));

				Iterator<String> i2 = objData.keySet().iterator();

				while (i2.hasNext()) {
					String string = i2.next();
					response.addValue(string, objData.get(string).toString());
				}

			} catch (ParseException e) {
				logger.severe("Error create JSON");
				e.printStackTrace();
			}

		} else {
			Socket socket = new Socket();
			socket.connect(new InetSocketAddress(host, port), timeout);
			socket.setSoTimeout(timeout);

			ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
			out.writeObject(request);
			out.flush();
			logger.info("Data sent");

			ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
			Object readObject = in.readObject();
			response = readObject.getClass().equals(Response.class) ? (Response) readObject : null;
			logger.info("Data received");

			out.close();
			in.close();
			socket.close();
		}
		return response;
	}

}

/**
 * 
 * Scale Connector
 * 
 * Copyright (C) Double Click Sistemas C.A. RIF: J-31576020-7 
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Double Click Sistemas C.A. Barquisimeto, Venezuela, http://dcs.net.ve
 * 
 */

package ve.net.dcs.sc.ui.controller;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ve.net.dcs.sc.component.Client;
import ve.net.dcs.sc.component.Request;
import ve.net.dcs.sc.component.RequestType;
import ve.net.dcs.sc.component.Response;
import ve.net.dcs.sc.ui.feature.SCUIFeature;
import ve.net.dcs.sc.ui.feature.SCUILocale;
import ve.net.dcs.sc.ui.util.HelperDate;
import ve.net.dcs.sc.ui.util.HelperFile;
import ve.net.dcs.sc.ui.view.ViewAbout;
import ve.net.dcs.sc.ui.view.ViewClientMode;
import ve.net.dcs.sc.ui.view.ViewWait;

/**
 * @author Double Click Sistemas C.A. - http://dcs.net.ve
 * @author Saul Pina - spina@dcs.net.ve
 */
public class ControllerViewClientMode implements ActionListener, ListSelectionListener, WindowListener {

	private ViewClientMode viewClientMode;
	private Client client;
	private Request request;
	private ViewWait viewWait;
	private static Logger logger = Logger.getLogger(ControllerViewClientMode.class.getName());
	private HashMap<String, String> parameters;
	private ArrayList<String> parametersIndex;
	private ArrayList<JMenuItem> manual;

	public ControllerViewClientMode() {
		parameters = new HashMap<String, String>();
		parametersIndex = new ArrayList<String>();
		viewClientMode = new ViewClientMode();
		viewClientMode.addListener(this);
		viewClientMode.getBtnDeleteParameter().setEnabled(false);

		File[] files = HelperFile.files(SCUIFeature.get("PATH_MANUAL"));
		manual = new ArrayList<JMenuItem>();
		for (int i = 0; files != null && i < files.length; i++) {
			JMenuItem item = new JMenuItem(files[i].getName());
			item.addActionListener(this);
			viewClientMode.getMenuItemManual().add(item);
			manual.add(item);
		}
		viewClientMode.setVisible(true);
	}

	public void close() {
		viewClientMode.dispose();
		System.exit(0);
	}

	private void sendRequest() {
		viewWait = new ViewWait();

		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					client = new Client(viewClientMode.getTxtHost().getText(), Integer.parseInt(viewClientMode.getTxtPort().getText()));
					client.setWebService(viewClientMode.getCbxWebService().isSelected());
					
					request = new Request();
					request.setType((RequestType) viewClientMode.getCmbRequestType().getSelectedItem());
					request.setDate(HelperDate.now());
					request.setParameters(parameters);
					

					Response response = client.sendRequest(request);

					viewClientMode.getLblSetResponseServerMessage().setText(response.getServerMessage());
					viewClientMode.getLblSetResponseDate().setText(HelperDate.format(response.getDate(), "yyyy-MM-dd H:mm:ss"));
					viewClientMode.getLblSetResponseStatus().setText(response.getStatus().toString());
					viewClientMode.getLblSetResponseStatusNotice().setText(response.getStatusNotice());

					viewClientMode.getLstModelResponseData().clear();
					Iterator<String> iterator = response.getData().keySet().iterator();
					while (iterator.hasNext()) {
						String key = (String) iterator.next();
						viewClientMode.getLstModelResponseData().addElement(String.format("[%s] = %s", key, response.getValue(key)));
					}
				} catch (SocketTimeoutException e) {
					logger.warning("Error connection timeout");
					JOptionPane.showMessageDialog(viewClientMode, SCUILocale.get("ViewClientMode.errorTimeOut"), "Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				} catch (Exception e) {					
					logger.warning("Error connecting to server");
					JOptionPane.showMessageDialog(viewClientMode, SCUILocale.get("ViewClientMode.errorMessageSend"), "Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}finally{
					viewWait.close();
				}
			}
		}).start();

		viewWait.display();
	}

	private void addParameter() {
		String parameterName = viewClientMode.getTxtParameterName().getText();
		String parameterValue = viewClientMode.getTxtParameterValue().getText();

		int index = parametersIndex.indexOf(parameterName);

		if (index < 0) {
			parametersIndex.add(parameterName);
			viewClientMode.getLstModelParameters().addElement(String.format("[%s] = %s", parameterName, parameterValue));
		} else {
			viewClientMode.getLstModelParameters().set(index, String.format("[%s] = %s", parameterName, parameterValue));
		}

		parameters.put(parameterName, parameterValue);

		viewClientMode.getTxtParameterName().setText("");
		viewClientMode.getTxtParameterValue().setText("");
	}

	private void deleteParameter() {
		int index = viewClientMode.getLstParameter().getSelectedIndex();
		parameters.remove(parametersIndex.get(index));
		parametersIndex.remove(index);
		viewClientMode.getLstModelParameters().remove(index);
		viewClientMode.getBtnDeleteParameter().setEnabled(false);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(viewClientMode.getMenuItemClose())) {
			close();
		} else if (e.getSource().equals(viewClientMode.getMenuItemAbout())) {
			new ViewAbout().setVisible(true);
		} else if (e.getSource().equals(viewClientMode.getMenuItemChangeMode())) {
			viewClientMode.dispose();
			new ControllerViewSelectMode();
		} else if (e.getSource().equals(viewClientMode.getBtnSend())) {
			sendRequest();
		} else if (e.getSource().equals(viewClientMode.getBtnAddParameter())) {
			addParameter();
		} else if (e.getSource().equals(viewClientMode.getBtnDeleteParameter())) {
			deleteParameter();
		} else {
			for (JMenuItem item : manual) {
				if (e.getSource().equals(item)) {
					try {
						Desktop.getDesktop().open(new File(SCUIFeature.get("PATH_MANUAL") + item.getText()));
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(viewClientMode, SCUILocale.get("ViewClientMode.notOpen"), "Error", JOptionPane.ERROR_MESSAGE);
						e1.printStackTrace();
					}
				}
			}
		}

	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (viewClientMode.getLstParameter().getSelectedValue() != null) {
			viewClientMode.getBtnDeleteParameter().setEnabled(true);
		}
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// NOTHING
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// NOTHING
	}

	@Override
	public void windowClosing(WindowEvent e) {
		close();
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// NOTHING
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// NOTHING
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// NOTHING
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// NOTHING
	}
}
